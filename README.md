# POC vagrant Nomad Consul Waypoint

- Waypoint: latest (0.2.0 on january 4th)
- nomad 1.0.1 (3 nodes)
- consul 1.9.1

## Install waypoint locally

```shell
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install waypoint
```

## Install Nomad and Consul on a local Vagrant-based 3-node cluster

Please make sure you have enough local resource! The VMs are small, yet they will reserve memory, disk, and CPU resources.

```shell
cd infra && vagrant up
```

When the 3 VMs are up:

```shell
for instance in $(seq 1 3)
do
vagrant ssh nomad-${instance} -- bash /vagrant/launch-${instance}.sh
done
# check if cluster is up and running
vagrant ssh nomad-1 -- nomad node status
exit
```

Nomad UI accessible at http://localhost:4646/, or at each VM IP address, e.g. http://172.16.1.101:4646/

## Deploy an app with Waypoint on Nomad

```shell
# install the Waypoint server job to Nomad
$ waypoint install -platform=nomad -nomad-dc=pondus-dc -accept-tos
# deploy example app
$ ./setup
$ cd waypoint-examples/nomad/nodejs
# First deploy:
$ waypoint init
$ waypoint up
```

## Conclusion

- poc failed at the last step (waypoint up) : deployment of example nodejs failed.
- As for commit 7c63567a3bc48a6179446913eafb916d97f7b25c, deployment works.
